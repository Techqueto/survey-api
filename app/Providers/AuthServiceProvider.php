<?php

namespace App\Providers;


use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use DB,Log;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

            $this->app['auth']->viaRequest('api', function ($request) {
                if($request->header('type') == 'website'){
                    if ($request->header('Authorization')) {
                        $header_value =  $request->header('Authorization');
                        Log::info(" header1 ". $header_value);
                        $value = explode(".",$header_value);
                        $api_token = $value[0];
                        $user_id = base64_decode($value[1]);
                    return DB::table('user_master')->where('token',  $api_token)->where('user_id',$user_id)->first();
                    };
                }
                if($request->header('type') == 'admin'){
                    if ($request->header('Authorization')) {
                        $header_value =  $request->header('Authorization');
                        Log::info("header 2" .$header_value);
                        $value = explode(".",$header_value);
                        $api_token = $value[0];
                        $user_id = base64_decode($value[1]);
                    return DB::table('admin_user_master')->where('api_token',  $api_token)->where('user_id',$user_id)->first();
                    };
            
            };
        });
    }
}