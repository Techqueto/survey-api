<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UserController extends Controller
{

    public function getAllUser()
    {
       $userData = DB::table('user_master')->get();
        return response()->json(array("data"=>$userData));
    }

    public function getUserDetail($id)
    {

        if(isset($id))
        {
            $userDetail = DB::table('user_master')->where('user_id', $id)->first();
            return response()->json(array('userDetil'=>$userDetail));
        }
        return response()->json(array('message'=>"Id is missing"));
    }

    public function createUser(Request $request)
    {
  
        if(isset($request->name))
        {
            $user_id = \Ramsey\Uuid\Uuid::uuid4()->toString();
            $api_token = sha1($user_id.time());
            $token = $api_token.".".base64_encode($user_id);
            $userData = DB::table("user_master")->insert(["user_id"=>$user_id,"user_name"=>$request->name,"user_email" => $request->email ,"user_mobile" => $request->mobile,'token'=> $api_token,"quiz_id"=>$request->quizId]);
          
           if($userData){
            return response()->json(array('user_id'=>$user_id,"token"=>$token)); 
           }
           return response()->json(array('msg'=>"user not inserted")); 
        }
        return response()->json(array('msg'=>"name is missing"));
    }

    public function updateUser( Request $request)
    {
        if(isset($request->user_id))
        {
        $user = DB::table('user_master')
                ->where('is_active', 1)
                ->where('user_id',$request->user_id)
                ->update(['is_active' => 0]);
                if($user){
                    return response()->json(array('msg'=>"user updated")); 
                   }
                   return response()->json(array('msg'=>"user not updated")); 
                }
                return response()->json(array('msg'=>"id is missing"));
    }

    public function getUserById(Request $request){
        if(isset($request->user_id))
        {
            $user = DB::table('user_master')
                        ->join("quiz_master","quiz_master.quiz_unique_id","user_master.quiz_id")
                        ->where('user_id',$request->user_id)
                        ->where('user_master.is_active', 1)
                        ->first();
            $api_token = $user->token;
            $token = $api_token.".".base64_encode($user->user_id);
            if($user){
                return response()->json(array("status"=>true,'data'=>array("user_id"=>$user->user_id,"user_name"=>$user->user_name,"token"=>$token,"quiz_name"=>$user->quiz_name,"quiz_desc"=>$user->quiz_desc)));
            }
            return response()->json(array("status"=>false,'msg'=>"user not updated"));   
        }
        return response()->json(array("status"=>false,'msg'=>"id is missing"));
    }

    public function adminLogin(Request $request){
        if(isset($request->email))
        {
            if(isset($request->password)){
                $password = md5($request->password);
                $userData = DB::table('admin_user_master')
                            ->where('email',$request->email)
                            ->where('password',$password)
                            ->where('is_active', 1)
                            ->get();
                if(count($userData) > 0)
                {
                    $user_id = \Ramsey\Uuid\Uuid::uuid4()->toString();
                    $api_token = sha1($user_id.time());
                    $token = $api_token.".".base64_encode($user_id);
                    $adminUserData  = DB::table('admin_user_master')
                                        ->where('is_active', 1)
                                        ->where('email',$request->email)
                                        ->update(['api_token' =>$api_token,"user_id"=>$user_id  ]);
                    $Data = DB::table('admin_user_master')
                                        ->where('api_token', $api_token)
                                        ->where('is_active', 1)
                                        ->get();
                    
                    return response()->json(array("status"=>true,"data"=>$Data,"token"=> $token ));

                }
                return response()->json(array("status"=>false,"msg"=>"email or password incorrect"));
            }
            return response()->json(array("status"=>false,"msg"=>"user pass is missing"));
        }

        return response()->json(array("status"=>false,"msg"=>"user email is missing"));
    }   


 
}