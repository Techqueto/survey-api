<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB;


class SurveyUserController extends Controller{

    public function createSurveyUser(Request $request){
        if($request->survey_user_name)
        {
        $survey_id = \Ramsey\Uuid\Uuid::uuid4()->toString();
        $surveyuserdata = DB::table('survey_user_master')->insert(['survey_user_master_id'=>$survey_id ,'user_id'=>$request->user_id,'survey_user_name'=>$request->survey_user_name,'survey_user_score' => $request->survey_user_score,'survey_total_score'=>$request->survey_total_score]);
           if($surveyuserdata){
            return response()->json(array("status"=>true,"data"=>$survey_id,"msg"=>"survey user created"));
           }
           return response()->json(array("status"=>true,"msg"=>"survey user not created"));
        }
            return response()->json(array('status'=>false,"msg"=>"survey name missing"));
    }

    public function getSurveyUser(){
        $surveyuserdata = DB::table('survey_user_master')->join('user_master','user_master.user_id','=','survey_user_master.user_id')->where('survey_user_master.is_active',1)->get();
        if($surveyuserdata){
            return response()->json(array("status"=>true,"data"=> $surveyuserdata ));
        }
        return response()->json(array('status'=>true,"msg"=>"no data found"));
    }

    public function getSurveyUserById(Request $request){
        if($request->user_id){
        $surveyuserdata = DB::table('survey_user_master')->where('user_id',$request->user_id)->where('is_active',1)->get();
        if($surveyuserdata){
            return response()->json(array("status"=>true,"data"=> $surveyuserdata ));
        }
        return response()->json(array('status'=>true,"msg"=>"no data found"));
        }
        return response()->json(array('status'=>false,"msg"=>"user id missing"));
    }

    public function addUserScore(Request $request){
        if($request->user_score){
            $surveyuserupdated = DB::table('survey_user_master')->where('user_id',$request->user_id)->update(['survey_user_score' => $request->user_score]);
            if($surveyuserupdated){
                return response()->json(array("status"=>true,"msg"=>"user score added"));
                }
                 return response()->json(array('status'=>true,"msg"=>"user is not  found"));
             }
    }

}
