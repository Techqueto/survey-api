<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class QuestionController extends Controller{


    //  Get Question by user
    public function getQuestionByUser(Request $request){

        if(isset($request->userId)){
            $questionData = DB::table('user_question_master')
                            ->where('user_id',$request->userId)
                            ->get();
            return response()->json(array("status"=>true,"data"=>$questionData));
        }
        return response()->json(array("message"=>"user id is missing"));
    }

    public function getAllQuestion(Request $request){
        $questionData = DB::table("question_master")
                        ->join("quiz_master","quiz_master.quiz_unique_id","question_master.quiz_unique_id")
                        ->where("question_master.is_active",1)
                        ->get();
            if($questionData){
                    return response()->json(array("status"=>true,"data"=>$questionData));
            } 
            return response()->json(array("message"=>"no data found"));             
    }

    public function getQuestionByQuiz(Request $request){
        if(isset($request->quiz_id)){
            $questionData = DB::table("question_master")
                            ->where("is_active",1)
                            ->where("quiz_unique_id",$request->quiz_id)
                            ->get();
            if($questionData){
                        return response()->json(array("status"=>true,"data"=>$questionData));
             }                   
        }
        return response()->json(array("message"=>"category id is missing"));
    }

    public function addUserQuestion(Request $request){
        if(isset($request->question) && isset($request->optionData) && isset($request->answer) && isset($request->userId)){
        $questionData = DB::table('user_question_master')->insert(["question"=>$request->question,"options"=>$request->optionData,"answer"=>$request->answer,"user_id"=>$request->userId]);
        if($questionData){
            return response()->json(array("status"=>true,"msg"=>"question added"));
         }
        }
        return response()->json(array("status"=>false,"msg"=>"data missing"));
    }

    public function createQuestion(Request $request){
        if(isset($request->question) && isset($request->options) && isset($request->quiz_id)){
        $questionData = DB::table('question_master')->insert(["question"=>$request->question,"options"=>$request->options,"quiz_unique_id"=>$request->quiz_id]);
        if($questionData){
            return response()->json(array("status"=>true,"msg"=>"question added"));
         }
        }
        return response()->json(array("status"=>false,"msg"=>"data missing"));
    }

    public function removeQuestion(Request $request){
        if(isset($request->question_id)){
        $questionData = DB::table('question_master')->where('question_id',$request->question_id)->update(['is_active'=>0]);
        if($questionData){
            return response()->json(array("status"=>true,"msg"=>"question rmeoved"));
         }
        }
        return response()->json(array("status"=>false,"msg"=>"data missing"));
    }

    public function getQuestionById($id){
        if(isset($id)){
            $questionData = DB::table('question_master')->where('question_id',$id)->where('is_active',1)->get();
          
            if($questionData){
                return response()->json(array("status"=>true,"data"=> $questionData));
            }
        }
            return response()->json(array("status"=>false,"msg"=>"something worng"));
    }

    public function updateQuestion(Request $request){
        if($request->question_id){
        $questionData = DB::table('question_master')->where('question_id',@$request->question_id)->update(["question"=>$request->question,"options"=>$request->options]);
      
        if($questionData){
            return response()->json(array("status"=>true,"msg"=> "updated the question"));
        }
    }
        return response()->json(array("status"=>false,"msg"=>"something worng"));
    }

}