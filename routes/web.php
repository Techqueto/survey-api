<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['prefix' => 'api','middleware' => 'auth'], function () use ($router) {

  //user api
  $router->get('get-all-user','UserController@getAllUser');
  $router->get('users/{id}','UserController@getUserDetail');
  $router->post('update-user', 'UserController@updateUser');
  

  //question api
  $router->post('get-user-question','QuestionController@getQuestionByUser');
  $router->post('add-user-question','QuestionController@addUserQuestion');


  //  survey api
  $router->get('get-all-survey-user','SurveyUserController@getSurveyUser');
  $router->post('get-survey-user','SurveyUserController@getSurveyUserById');
  $router->post('create-survey-user','SurveyUserController@createSurveyUser');
 


  // dashboard
  // get all question
  $router->get('get-all-question','QuestionController@getAllQuestion');
  $router->post('create-question','QuestionController@createQuestion');
  $router->post('remove-question','QuestionController@removeQuestion');
  $router->get('get-question/{id}','QuestionController@getQuestionById');
  $router->post('update-question','QuestionController@updateQuestion');

  
  // category api
  $router->post('create-category','CategoryController@createCategory');
  $router->post('remove-category', 'CategoryController@removeCategory');
  $router->get('get-admin-all-category','CategoryController@getCategoryList');
    
  // quiz api
  $router->post('create-quiz','QuizController@createQuiz');
  $router->post('remove-quiz','QuizController@removeQuiz');
  $router->post('update-quiz','QuizController@updateQuiz');
  $router->get('get-all-quiz','QuizController@getAllQuiz');
  $router->get('get-quiz/{id}','QuizController@getQuizById');

});

$router->group(['prefix' => 'api'], function () use ($router) {
$router->post('create-user','UserController@createUser');
$router->post('get-userId', 'UserController@getUserById');
$router->get('get-all-category','CategoryController@getCategoryList');
$router->post('get-question-category','QuestionController@getQuestionByCategory');
$router->post('get-question-quiz','QuestionController@getQuestionByQuiz');
$router->post('get-quiz-list','QuizController@getQuizList');




// adim login api
$router->post('admin-user-login','UserController@adminLogin');
});

