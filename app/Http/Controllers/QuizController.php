<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB,Log;

class QuizController extends Controller
{
    public function createQuiz(Request $request)
    {
        if($request->quiz_name){
        $quiz_id = \Ramsey\Uuid\Uuid::uuid4()->toString();
         $quizData =   DB::table('quiz_master')->insert(["quiz_unique_id"=>$quiz_id,"quiz_name"=>$request->quiz_name,"category_unique_id"=>$request->category_id,"quiz_desc"=>@$request->quiz_desc]);
         Log::info($quizData);
            if($quizData){
               
                return response()->json(array("status"=>true,"msg"=>"quiz is added"));
            }
            return response()->json(array("status"=>true,"msg"=>"quiz not added"));
        }
        return response()->json(array("status"=>false,"msg"=>"Quiz name missing"));
    }


    public function getQuizList(Request $request){
        $quizData = DB::table('quiz_master')->where('is_active',1)->where('category_unique_id',$request->category_id)->get();

        if(count($quizData) > 0)
        {
            return response()->json(array("status"=>true,"data"=>$quizData));
        }
        return response()->json(array("status"=>true,"msg"=>"no data found"));
    }


    public function removeQuiz(Request $request){
        if($request->quiz_id){
            $quizData  = DB::table('quiz_master')
            ->where('quiz_unique_id',$request->quiz_id)
            ->update(['is_active'=>0]);
            if($quizData){
                return response()->json(array("status"=>true,"msg"=>"quiz is removed"));
            }
            return response()->json(array("status"=>false,"msg"=>"quiz is not removed"));
        }
      
        return response()->json(array("status"=>false,"msg"=>"quiz id missing"));
    }

    public function getAllQuiz(){
        $quizData = DB::table('quiz_master')->join('category_master','quiz_master.category_unique_id','=','category_master.category_unique_id')->select('quiz_master.*','category_master.*')->where('quiz_master.is_active',1)->get();
        Log::info($quizData);
        if($quizData){
            return response()->json(array("status"=>true,"data"=> $quizData));
        }
        return response()->json(array("status"=>false,"msg"=>"something worng"));
    }

    public function getQuizById($id){
        if(isset($id)){
        $quizData = DB::table('quiz_master')->where('is_active',1)->where('quiz_id',$id)->get();
        Log::info($quizData);
        if($quizData){
            return response()->json(array("status"=>true,"data"=> $quizData));
        }
    }
        return response()->json(array("status"=>false,"msg"=>"something worng"));
    }

    
    public function updateQuiz(Request $request){
        if($request->quiz_id){

        
        $quizData = DB::table('quiz_master')->where('quiz_id',@$request->quiz_id)->update(["quiz_name"=>@$request->quiz_name,"quiz_desc"=>@$request->quiz_desc]);
        Log::info($quizData);
        if($quizData){
            return response()->json(array("status"=>true,"msg"=> "updated the quiz"));
        }
    }
        return response()->json(array("status"=>false,"msg"=>"something worng"));
    }
}
