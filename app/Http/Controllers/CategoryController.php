<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB,Log;


class CategoryController extends Controller{

    public function getCategoryList(){
        $categoryData = DB::table('category_master')->where('is_active',1)->get();
        if(isset($categoryData)){
            return response()->json(array("status"=>true,"data"=>$categoryData));
        }
        return response()->json(array("status"=>false,"msg"=>"no data Found"));
    }

    public function createCategory(Request $request){
        if(isset($request->category_name)){
        $category_id = \Ramsey\Uuid\Uuid::uuid4()->toString();
        Log::info($category_id);
        // $categoryData = DB::raw("insert into category_master(category_id,category_name) values($category_id,$request->category_name)")->toSql();
            $categoryData = DB::table('category_master')->insert(["category_unique_id"=>$category_id,"category_name"=>$request->category_name]);
        if($categoryData){
            // return redirect('category.html');
            return response()->json(array("status"=>true,"msg"=>"category added",' $categoryData'=> $categoryData));
         }
        return response()->json(array("status"=>false,"msg"=>"category not added")); 
        }
        return response()->json(array("status"=>false,"msg"=>"Category Name is missing"));
    }

    public function removeCategory(Request $request){
            if(isset($request->category_id))
            {
          $categoryData = DB::table('category_master')
                            ->where('is_active', 1)
                            ->where('category_id',$request->category_id)
                            ->update(['is_active' => 0]);
                    if($categoryData){
                        return response()->json(array("status"=>true,'msg'=>"category removed")); 
                       }
                       return response()->json(array("status"=>false,'msg'=>"category not updated")); 
                    }
                    return response()->json(array("status"=>false,'msg'=>"category id is missing"));
        
    }
}

